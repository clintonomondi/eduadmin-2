import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueLoading from 'vuejs-loading-plugin'
import VueTelInput from 'vue-tel-input';

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLoading)
Vue.use(VueToast);
Vue.use(VueTelInput);


import App from './view/App'
import Login from './pages/login'
import Forgotpass from './pages/forgotpass'
import Password from './pages/password'
import Category from './pages/category'
import Teachers from './pages/teachers'
import Transactions from './trans/mpesa'
import Payment from './trans/payment'
import Taxes from './pages/taxes'
import Addcategory from './pages/addcategory'

import Home from './pages/home'
import Editcategory from './pages/editcategory'
import Classes from './pages/classes'
import Subject from './pages/subjects'
import Clients from './pages/clients'
import Users from './pages/users'




const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/clients',
            name: 'clients',
            component: Clients
        },
        {
            path: '/users',
            name: 'users',
            component: Users
        },
        {
            path: '/password',
            name: 'password',
            component: Password
        },
        {
            path: '/',
            name: 'login',
            component: Login
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/forgotpass',
            name: 'forgotpass',
            component: Forgotpass
        },
        {
            path: '/category',
            name: 'category',
            component: Category
        },
        {
            path: '/teachers',
            name: 'teachers',
            component: Teachers
        },
        {
            path: '/mpesa/transactions',
            name: 'transactions',
            component: Transactions
        },
        {
            path: '/payment/transactions',
            name: 'payment',
            component: Payment
        },
        {
            path: '/taxes',
            name: 'taxes',
            component: Taxes
        },
        {
            path: '/category/add',
            name: 'addcategory',
            component: Addcategory
        },

        {
            path: '/home',
            name: 'home',
            component: Home
        },

        {
            path: '/editcategory/:id',
            name: 'editcategory',
            component: Editcategory
        },
        {
            path: '/classes/:id',
            name: 'classes',
            component: Classes
        },
        {
            path: '/subjects/:id',
            name: 'subjects',
            component: Subject
        },




    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,

});