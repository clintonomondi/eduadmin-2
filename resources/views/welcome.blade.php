<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Edubora</title>
      
      <!-- Favicon -->
      <!-- <link rel="shortcut icon" href="/images/logo2.png" /> -->
      
      <link rel="stylesheet" href="/assets/css/backend-plugin.css">
      <link rel="stylesheet" href="/assets/css/backend.css%3Fv=1.0.0.css"> 
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

     </head>
    <body >
        <!-- loader Start -->
   <!-- <div id="loading">
          <div id="loading-center">
          </div>
    </div>
     loader END -->
    <div id="app">
    <app></app>
   </div>
    <script src="{{ mix('js/app.js') }}"></script>
       

    <script src="/assets/js/backend-bundle.min.js"></script>
    <!-- Chart Custom JavaScript -->
    <script src="/assets/js/customizer.js"></script>
    
    <script src="/assets/js/sidebar.js"></script>
    
    <!-- Flextree Javascript-->
    <script src="/assets/js/flex-tree.min.js"></script>
    <script src="/assets/js/tree.js"></script>
    
    <!-- Table Treeview JavaScript -->
    <script src="/assets/js/table-treeview.js"></script>
    
    <!-- SweetAlert JavaScript -->
    <script src="/assets/js/sweetalert.js"></script>
    
    <!-- Vectoe Map JavaScript -->
    <script src="/assets/js/vector-map-custom.js"></script>
    
    <!-- Chart Custom JavaScript -->
    <script src="/assets/js/chart-custom.js"></script>
    <script src="/assets/js/charts/01.js"></script>
    <script src="/assets/js/charts/02.js"></script>
    
    <!-- slider JavaScript -->
    <script src="/assets/js/slider.js"></script>
    
    <!-- Emoji picker -->
    <script src="/assets/vendor/emoji-picker-element/index.js" type="module"></script>
    
    
    <!-- app JavaScript -->
    <script src="/assets/js/app.js"></script> 



    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.min.js" integrity="sha512-RGbSeD/jDcZBWNsI1VCvdjcDULuSfWTtIva2ek5FtteXeSjLfXac4kqkDRHVGf1TwsXCAqPTF7/EYITD0/CTqw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.esm.js" integrity="sha512-rX92e1gJcy6G+ivRwDY5NnrDdGz37qBHqhNgDB9b9oT83N+vcKOs7GCcDTvKz/mFanYSTz+EoRi8SGlMOd3MoQ==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/chart.js" integrity="sha512-LlFvdZpYhQdASf4aZfSpmyHD6+waYVfJRwfJrBgki7/Uh+TXMLFYcKMRim65+o3lFsfk20vrK9sJDute7BUAUw==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.js" integrity="sha512-334wuK4vkaONVysNjemyZ0HZDlNvZnjAEvrOu4LFn4fCrCfzWJjFG7wePPfZtW7bGmWvjN/r0RkwiaQ67+hsNg==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.1.0/helpers.esm.min.js" integrity="sha512-5XGk12SGIo2btywdra6Pg6M+mHRnso/oZE4TVQ9+FAqHxG2dN8FHk4/fw+XXd9xUFSd0xatlMjEEBbwFgq7THw==" crossorigin="anonymous"></script>
    </body>
</html>
